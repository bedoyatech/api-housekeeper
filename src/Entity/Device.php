<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ApiResource()
 * @ORM\Entity(repositoryClass="App\Repository\DeviceRepository")
 */
class Device
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=100, nullable=true)
     */
    private $name;

    /**
     * @ORM\Column(type="string", length=60)
     */
    private $mac_address;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\DeviceType")
     */
    private $device_type;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Account", inversedBy="devices")
     * @ORM\JoinColumn(nullable=false)
     */
    private $account;

    /**
     * @return int|null
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @return string|null
     */
    public function getName(): ?string
    {
        return $this->name;
    }

    /**
     * @param string|null $name
     * @return $this
     */
    public function setName(?string $name): self
    {
        $this->name = $name;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getMacAddress(): ?string
    {
        return $this->mac_address;
    }

    /**
     * @param string $mac_address
     * @return $this
     */
    public function setMacAddress(string $mac_address): self
    {
        $this->mac_address = $mac_address;

        return $this;
    }

    /**
     * @return DeviceType|null
     */
    public function getDeviceType(): ?DeviceType
    {
        return $this->device_type;
    }

    /**
     * @param DeviceType|null $device_type
     * @return $this
     */
    public function setDeviceType(?DeviceType $device_type): self
    {
        $this->device_type = $device_type;

        return $this;
    }

    public function getAccount(): ?Account
    {
        return $this->account;
    }

    public function setAccount(?Account $account): self
    {
        $this->account = $account;

        return $this;
    }
}
